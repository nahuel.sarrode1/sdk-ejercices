const DYNAMODB = require('aws-sdk/clients/dynamodb');
const dynamodb = new DYNAMODB.DocumentClient({ region: 'us-east-1' });

// generate a ramdom number
function randomNumber(minimum, maximum) {
	return Math.round(Math.random() * (maximum - minimum) + minimum);
}

function calculateAge(birthday) {
	// birthday is a string in format YYYYMMDD
	const birthDate = new Date(birthday);
	const ageDifMs = Date.now() - birthDate.getTime();
	const ageDate = new Date(ageDifMs);

	return Math.abs(ageDate.getUTCFullYear() - 1970);
}

const createCard = (birthDay) => {
	return {
		cardNumber: `${randomNumber(0000, 9999)} - ${randomNumber(
			0000,
			9999
		)} - ${randomNumber(0000, 9999)} - ${randomNumber(0000, 9999)}`,
		expirationDate: `${randomNumber(01, 12)}/${randomNumber(21, 35)}`,
		securityCode: `${randomNumber(000, 999)}`,
		type: calculateAge(birthDay) > 30 ? 'Gold' : 'Classic',
	};
};

const saveCard = async (
	{ dni, name, lastname, date },
	{ cardNumber, expirationDate, securityCode, type }
) => {
	try {
		const paramsDb = {
			TableName: 'nahuelsarrode-clients',
			Item: {
				dni,
				name,
				lastname,
				date,
				cardNumber,
				expirationDate,
				securityCode,
				type,
			},
		};

		return await dynamodb.put(paramsDb).promise();
	} catch (error) {
		console.log(`Error creating card: ${error}`);
		throw error;
	}
};

exports.handler = async (event) => {
	const queue = event.Records.map((record) => record.body);

	for (const item of queue) {
		let message = JSON.parse(item);
		let body = JSON.parse(message.Message);

		const card = createCard(body.date);

		await saveCard(body, card);
	}

	return {
		statusCode: 200,
		body: JSON.stringify('Card created'),
	};
};
