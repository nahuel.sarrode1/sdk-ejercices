const DYNAMODB = require('aws-sdk/clients/dynamodb');
const dynamoDb = new DYNAMODB.DocumentClient({ region: 'us-east-1' });

// Change the original year in all dates makes
// more easy compare different dates
// and in this case we only need month and day to calculate station.
const chooseGift = (date) => {
	let birthday = date;
	const month = parseInt(birthday.split('-')[1]);
	const day = parseInt(birthday.split('-')[2]);
	birthday = new Date(`2022-${month}-${day}`);

	let gift;
	if (birthday >= new Date('2021-12-21') && birthday < new Date('2022-03-20'))
		gift = 'Shirt'; // summer
	if (birthday >= new Date('2022-03-20') && birthday < new Date('2022-06-21'))
		gift = 'Sweater'; // fall
	if (birthday >= new Date('2022-06-21') && birthday < new Date('2022-09-21'))
		gift = 'Hoody'; // winter
	if (birthday >= new Date('2022-09-21') && birthday < new Date('2022-12-21'))
		gift = 'T-Shirt'; // spring

	return gift;
};

const saveGift = async (client) => {
	try {
		const paramsDb = {
			TableName: 'nahuelsarrode-clients',
			Item: {
				dni: client.dni,
				name: client.name,
				lastname: client.lastname,
				date: client.date,
				cardNumber: client.cardNumber,
				expirationDate: client.expirationDate,
				securityCode: client.securityCode,
				type: client.type,
				gift: client.gift,
			},
		};

		return await dynamoDb.put(paramsDb).promise();
	} catch (error) {
		console.log(`Error choosing gift: ${error}`);
		throw error;
	}
};

const getClient = async ({ dni }) => {
	try {
		const dbParams = {
			TableName: 'nahuelsarrode-clients',
			Key: { dni },
		};

		return await dynamoDb.get(dbParams).promise();
	} catch (error) {
		console.log(`Error getting client by dni: ${error}`);
		throw error;
	}
};

exports.handler = async (event) => {
	try {
		const queue = event.Records.map((item) => item.body);

		for (const item of queue) {
			let message = JSON.parse(item);
			let body = JSON.parse(message.Message);

			let client = await getClient(body);
			let gift = chooseGift(client.Item.date);
			client.Item.gift = gift;
			await saveGift(client.Item);
		}

		return {
			statusCode: 200,
			body: JSON.stringify('Gift assigned correctly'),
		};
	} catch (error) {
		console.log(`Error assigning gift ${error}`);
		throw error;
	}
};
